//
//  CalculatorViewController.swift
//  CryptoCurrency
//
//  Created by Юрий on 2/12/19.
//  Copyright © 2019 Юрий. All rights reserved.
//

import UIKit

class CalculatorViewController: UIViewController {
    
    @IBOutlet weak var MSLabel: UILabel!
    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet weak var outputTextField: UITextField!
    
    @IBAction func swapCurr(_ sender: UIButton) {
        toggleCulc()
    }
    
    
    var cryptoCurr = CryptoCurrence()
    var mark = ""
    var makeOper = false

    @IBAction func doCalc(_ sender: Any) {
        if let input = inputTextField.text {
            let sum = Double(input)
            var result = 0.0
            if makeOper {
                result = sum! / cryptoCurr.price
            } else {
                result = sum! * cryptoCurr.price
            }
            outputTextField.text = String(result)
        }
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        inputTextField.placeholder = cryptoCurr.symbol
        inputTextField.text = ""
        outputTextField.placeholder = mark
        outputTextField.text = ""
        MSLabel.text = "1 \(cryptoCurr.symbol) = \(cryptoCurr.price) \(mark) "
    }
    
    func toggleCulc() {
        if !makeOper {
            inputTextField.placeholder = mark
            inputTextField.text = ""
            outputTextField.placeholder = cryptoCurr.symbol
            outputTextField.text = ""
            makeOper = true
            
        } else {
            inputTextField.placeholder = cryptoCurr.symbol
            inputTextField.text = ""
            outputTextField.placeholder = mark
            outputTextField.text = ""
            makeOper = false
        }
    }

}
