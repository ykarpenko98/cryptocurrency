//
//  APIController.swift
//  CryptoCurrency
//
//  Created by Юрий on 2/9/19.
//  Copyright © 2019 Юрий. All rights reserved.
//

import Foundation
import Alamofire

class API {

    class func getCryptoCurrencies(completion: @escaping ([CryptoCurrence]?) -> Void, by mark: String) {
        AF.request("https://api.coinmarketcap.com/v1/ticker/?convert=\(mark)&limit=20").responseJSON {
            response in
            if let result = response.result.value {
                var res = [CryptoCurrence]()
                switch mark {
                case "USD": res = parseCryptUSD(result: result)
                case "EUR": res = parseCryptEUR(result: result)
                case "RUB": res = parseCryptRUB(result: result)
                default: break
                }
                
                completion(res)
            }
        }
    }
    
    class func getGraphCoordinate(completion: @escaping ([Double]) -> Void, by symb: String, by mark: String = "USD") {
        var coords = [Double]()
        AF.request("https://min-api.cryptocompare.com/data/histoday?fsym=\(symb)&tsym=\(mark)&limit=180&aggregate=1").responseJSON {
            response in
            if let result = response.result.value {
                if let JSON = result as? NSDictionary {
                    if let data = JSON["Data"] as? [NSDictionary]{
                        for item in data {
                            coords.append(item["close"] as! Double)
                        }
                    }
                    
                }
                completion(coords)
            }
            
        }
    }
    
}

private func parseCryptUSD(result: Any) -> [CryptoCurrence] {
    var cryptoCurrencies = [CryptoCurrence]()
    let JSON = result as! NSArray
    if let cryptArr = JSON as? [NSDictionary?] {
        for item in cryptArr {
            var cryptoCurrence = CryptoCurrence()
            cryptoCurrence.id = item!["id"] as! String
            cryptoCurrence.name = item!["name"] as! String
            cryptoCurrence.percentChange = Double(item!["percent_change_24h"] as! String)!
            cryptoCurrence.percentChange1h = Double(item!["percent_change_1h"] as! String)!
            cryptoCurrence.percentChange7d = Double(item!["percent_change_7d"] as! String)!
            cryptoCurrence.rank = item!["rank"] as! String
            cryptoCurrence.symbol = item!["symbol"] as! String
            cryptoCurrence.volume24h = Double(item!["24h_volume_usd"] as! String)!
            cryptoCurrence.totalSupply = Double(item!["total_supply"] as! String)!
            cryptoCurrence.price = Double(item!["price_usd"] as! String)!
            cryptoCurrence.marketCap = Double(item!["market_cap_usd"] as! String)!
            
            cryptoCurrencies.append(cryptoCurrence)
        }
    }
    return cryptoCurrencies
}

private func parseCryptEUR(result: Any) -> [CryptoCurrence] {
    var cryptoCurrencies = [CryptoCurrence]()
    let JSON = result as! NSArray
    if let cryptArr = JSON as? [NSDictionary?] {
        for item in cryptArr {
            var cryptoCurrence = CryptoCurrence()
            cryptoCurrence.id = item!["id"] as! String
            cryptoCurrence.name = item!["name"] as! String
            cryptoCurrence.percentChange = Double(item!["percent_change_24h"] as! String)!
            cryptoCurrence.percentChange1h = Double(item!["percent_change_1h"] as! String)!
            cryptoCurrence.percentChange7d = Double(item!["percent_change_7d"] as! String)!
            cryptoCurrence.rank = item!["rank"] as! String
            cryptoCurrence.symbol = item!["symbol"] as! String
            cryptoCurrence.totalSupply = Double(item!["total_supply"] as! String)!
            cryptoCurrence.volume24h = Double(item!["24h_volume_eur"] as! String)!
            cryptoCurrence.marketCap = Double(item!["market_cap_eur"] as! String)!
            cryptoCurrence.price = Double(item!["price_eur"] as! String)!
            
            cryptoCurrencies.append(cryptoCurrence)
        }
    }
    return cryptoCurrencies
}

private func parseCryptRUB(result: Any) -> [CryptoCurrence] {
    var cryptoCurrencies = [CryptoCurrence]()
    let JSON = result as! NSArray
    if let cryptArr = JSON as? [NSDictionary?] {
        for item in cryptArr {
            var cryptoCurrence = CryptoCurrence()
            cryptoCurrence.id = item!["id"] as! String
            cryptoCurrence.name = item!["name"] as! String
            cryptoCurrence.percentChange = Double(item!["percent_change_24h"] as! String)!
            cryptoCurrence.percentChange1h = Double(item!["percent_change_1h"] as! String)!
            cryptoCurrence.percentChange7d = Double(item!["percent_change_7d"] as! String)!
            cryptoCurrence.rank = item!["rank"] as! String
            cryptoCurrence.symbol = item!["symbol"] as! String
            cryptoCurrence.volume24h = Double(item!["24h_volume_rub"] as! String)!
            cryptoCurrence.totalSupply = Double(item!["total_supply"] as! String)!
            cryptoCurrence.price = Double(item!["price_rub"] as! String)!
            cryptoCurrence.marketCap = Double(item!["market_cap_rub"] as! String)!
            
            cryptoCurrencies.append(cryptoCurrence)
        }
    }
    return cryptoCurrencies
}




