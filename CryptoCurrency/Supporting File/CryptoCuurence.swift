//
//  CryptoCuurence.swift
//  CryptoCurrency
//
//  Created by Юрий on 2/9/19.
//  Copyright © 2019 Юрий. All rights reserved.
//

import Foundation

struct CryptoCurrence {
    var id = ""
    var name = ""
    var symbol = ""
    var price = 0.0
    var percentChange1h = 0.0
    var percentChange = 0.0
    var percentChange7d = 0.0
    var rank = ""
    var volume24h = 0.0
    var totalSupply = 0.0
    var marketCap = 0.0
    
}
