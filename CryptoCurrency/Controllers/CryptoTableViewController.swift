//
//  CryptoTableViewController.swift
//  CryptoCurrency
//
//  Created by Юрий on 2/8/19.
//  Copyright © 2019 Юрий. All rights reserved.
//

import UIKit

class CryptoTableViewController: UITableViewController {
    
    @IBOutlet weak var chooseOutlet: UIBarButtonItem!
    var mark: String {
        get {
            if let result = UserDefaults.standard.string(forKey: "currenceType") {
                return result
            }
            return "USD"
        }
        
        set {
            UserDefaults.standard.set(newValue, forKey: "currenceType")
            chooseOutlet.title = newValue
            API.getCryptoCurrencies(completion: { result in
                self.cryptoCurrencies = result!
            }, by: mark)
        }
    }
    
    var cryptoCurrencies = [CryptoCurrence]() {
        didSet {
            tableView.reloadData()

        }
    }

    @IBAction func chooseCurrence(_ sender: UIBarButtonItem) {
        let actionTitle = NSLocalizedString("Choose currence", comment: "")
        
        let optionMenu = UIAlertController(title: nil, message: actionTitle, preferredStyle: .actionSheet)
        let usdAction = UIAlertAction(title: "USD", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.mark = "USD"
        })
        let rubAction = UIAlertAction(title: "RUB", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.mark = "RUB"
        })

        let eurAction = UIAlertAction(title: "EUR", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.mark = "EUR"
        })
        
        optionMenu.addAction(usdAction)
        optionMenu.addAction(eurAction)
        optionMenu.addAction(rubAction)
        self.present(optionMenu, animated: true, completion: nil)

    }
    override func viewDidLoad() {
        super.viewDidLoad()
        chooseOutlet.title = mark
        API.getCryptoCurrencies(completion: { result in
            self.cryptoCurrencies = result!
        }, by: mark)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cryptoCurrencies.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CryptoCell", for: indexPath) as! CryptTableViewCell
        
        let content = cryptoCurrencies[indexPath.row]
        cell.nameLabel?.text = content.name
        cell.priceLabel?.text = String(content.price)
        cell.percentChangeLabel?.text = String(content.percentChange)
        if content.percentChange > 0 {
            cell.percentChangeLabel.backgroundColor = UIColor(displayP3Red: 0, green: 50, blue: 0, alpha: 1)
        } else if content.percentChange < 0 {
            cell.percentChangeLabel.backgroundColor = UIColor(displayP3Red: 50, green: 0, blue: 0, alpha: 1)
        }
        else {
            cell.percentChangeLabel.text = "--"
        }
        
        return cell
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowDetailCrypt" {
            if let indexPath = tableView.indexPath(for: sender as! UITableViewCell){
                let nc = segue.destination as! UINavigationController
                let controller = nc.topViewController as! DetailCryptoViewController
                controller.cryptoCurrence = cryptoCurrencies[indexPath.row]
                controller.mark = mark
            }
            
        }
    }

}
