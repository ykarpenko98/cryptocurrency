//
//  CryptoDetailViewController.swift
//  CryptoCurrency
//
//  Created by Юрий on 2/9/19.
//  Copyright © 2019 Юрий. All rights reserved.
//

import UIKit
import Charts

class DetailCryptoViewController: UIViewController {
    
    var cryptoCurrence = CryptoCurrence()
    var mark = ""
    
    var coords = [Double]() {
        didSet {
            setChartValue(coords)
            lineChartView.isHidden = false
        }
    }
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    @IBOutlet weak var percentChange7dLabel: UILabel!
    @IBOutlet weak var totalSupplyLabel: UILabel!
    @IBOutlet weak var volumeLabel: UILabel!
    @IBOutlet weak var rabkLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var percentChangeLabel: UILabel!
    @IBOutlet weak var symbolLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var lineChartView: LineChartView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lineChartView.isHidden = true
        API.getGraphCoordinate(completion: { coords in
            self.coords = coords
        }, by: cryptoCurrence.symbol, by: mark)
        setLabels()
        print(cryptoCurrence)
    }
    
    func setChartValue(_ coords: [Double]) {
        var values = [ChartDataEntry]()
        var i = 0
        for item in coords {
            let val = ChartDataEntry(x: Double(i), y: item)
            values.append(val)
            i += 1
        }
        
        let set1 = LineChartDataSet(values: values, label: "DataSet1")
        set1.drawCirclesEnabled = false
        let data = LineChartData(dataSet: set1)
        data.setDrawValues(false)
        lineChartView.backgroundColor = UIColor(displayP3Red: 0, green: 0, blue: 0, alpha: 1)
        
        lineChartView.data = data
        lineChartView.xAxis.labelTextColor = UIColor(displayP3Red: 255, green: 255, blue: 255, alpha: 1)
        lineChartView.leftAxis.labelTextColor = UIColor(displayP3Red: 255, green: 255, blue: 255, alpha: 1)
    }
    
    func setLabels() {
        nameLabel.text = cryptoCurrence.name
        rabkLabel.text = cryptoCurrence.rank
        priceLabel.text = String(cryptoCurrence.price)
        percentChangeLabel.text = String(cryptoCurrence.percentChange)
        symbolLabel.text = cryptoCurrence.symbol
        volumeLabel.text = String(cryptoCurrence.volume24h)
        totalSupplyLabel.text = String(cryptoCurrence.totalSupply)
        percentChange7dLabel.text = String(cryptoCurrence.percentChange7d)
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowCalc" {
            let vc = segue.destination as! CalculatorViewController
            vc.cryptoCurr = cryptoCurrence
            vc.mark = mark
        }
    }
}
