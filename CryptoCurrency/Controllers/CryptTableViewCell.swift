//
//  CryptTableViewCell.swift
//  CryptoCurrency
//
//  Created by Юрий on 2/9/19.
//  Copyright © 2019 Юрий. All rights reserved.
//

import UIKit

class CryptTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var percentChangeLabel: UILabel!

}
